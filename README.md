# aungmyatkyaw-cicd

Welcome to the Gitlab CI/CD Remote Template Group! This group houses various projects related to GitLab CI/CD, including base images for GitLab CI/CD jobs, example frontend angular and backend springboot and .NET projects using the GitLab CI remote template, and the remote GitLab CI project itself.

## Projects

### Base Images for GitLab CI/CD Jobs

This project provides base images for various types of jobs used in GitLab CI pipelines. These images are optimized and pre-configured with commonly used tools and dependencies, allowing for faster and more efficient CI/CD workflows.
- [Alpine](https://gitlab.com/aungmyatkyaw-cicd/base-images/alpine)
- [Container Scanning](https://gitlab.com/aungmyatkyaw-cicd/base-images/container-scanning)
- [Deploy](https://gitlab.com/aungmyatkyaw-cicd/base-images/deploy)
- [DevSecOps-Maven](https://gitlab.com/aungmyatkyaw-cicd/base-images/devsecops-maven)
- [Dotnet](https://gitlab.com/aungmyatkyaw-cicd/base-images/dotnet)
- [Git](https://gitlab.com/aungmyatkyaw-cicd/base-images/git)
- [Kaniko](https://gitlab.com/aungmyatkyaw-cicd/base-images/kaniko)
- [Maven](https://gitlab.com/aungmyatkyaw-cicd/base-images/maven)
- [Node](https://gitlab.com/aungmyatkyaw-cicd/base-images/node)
- [OpenJDK](https://gitlab.com/aungmyatkyaw-cicd/base-images/openjdk)

### GitLab CI/CD Remote Template

The GitLab CI/CD remote template project serves as a template repository for configuring CI/CD pipelines in other projects hosted on GitLab. It provides a standardized CI/CD configuration that can be easily integrated into other repositories, enabling quick setup of CI/CD pipelines without starting from scratch.

**Repository URL**: [Gitlab CI-CD Remote Template](https://gitlab.com/aungmyatkyaw-cicd/gitlab-ci-cd-remote-template)

### Example Frontend Project

The example frontend project demonstrates how to utilize the GitLab CI/CD remote template for setting up CI/CD pipelines in a frontend application. It includes configuration files and scripts tailored for building, testing, and deploying frontend code using GitLab CI/CD.

**Repository URL**: [Example Frontend Angular Project](https://gitlab.com/aungmyatkyaw-cicd/sample-projects/angular-frontend-example)

### Example Backend Project

The example backend project showcases the implementation of GitLab CI/CD pipelines in a backend application. It illustrates the usage of the GitLab CI/CD remote template for automating tasks such as testing, linting, and deploying backend services.

**Repository URL**: [Example Backend Springboot Project](https://gitlab.com/aungmyatkyaw-cicd/sample-projects/spring-boot-3-rest-api-example)

### Example .NET Project

The example .NET project demonstrates how to integrate the GitLab CI remote template with a .NET application. It showcases CI/CD pipelines for building, testing, and deploying .NET applications using GitLab CI.

**Repository URL**: [Example .NET Project](https://gitlab.com/aungmyatkyaw-cicd/sample-projects/dotnet-6-rest-api-example)

### Terraform DevSecOps Testing Environment

Automates the provisioning of infrastructure components for a DevSecOps testing environment on AWS, Cloudflare, and GitLab.

**Repository URL**: [Terraform DevSecOps Project](https://gitlab.com/aungmyatkyaw-cicd/sample-projects/devsecops-gitlab-terraform)

## Usage

To use the GitLab CI/CD remote template in your projects, follow the instructions provided in the [README](https://gitlab.com/aungmyatkyaw-cicd/gitlab-ci-cd-remote-template/-/blob/main/README.md?ref_type=heads) of the template repository.

### AWS Parameter Store Configuration

AWS Parameter Store is integrated as a configuration management solution for deployments. Configuration values from AWS Parameter Store are retrieved using AWS CLI v2 and using them in CI/CD pipelines before deploying with [Deploy](https://gitlab.com/aungmyatkyaw-cicd/base-images/deploy) image.

## Contributing

I welcome contributions to any of the projects within this group! Feel free to submit bug reports, feature requests, or pull requests through the respective GitLab repositories.

## Support

For support or assistance, please reach out to the project maintainers via the issue tracker on the respective GitLab repositories.

## Acknowledgements

I would like to thank all contributors and users who have helped improve and maintain the projects within this group.
